const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types
 
const articleSchema = new mongoose.Schema({
    articleImage:{
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    heading: {
        type: String,
        required: true
    },
    description:{
        type: String,
    },
    category: {
        type: String,
        required:  true
    },
    Author: {
        type: ObjectId,
        ref: "User"
    }, 
    likes: [
        {
          type: ObjectId,
          ref: "User"
        }
    ],
    comments:[{text: String, postedBy:{type: ObjectId ,ref: 'User'}}],
    createdAt: {
        type: Date,
        default:Date.now
    },
})
 
module.exports = mongoose.model('Article', articleSchema)