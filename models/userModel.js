const mongoose = require('mongoose')

const jwt=require('jsonwebtoken');
const bcrypt=require('bcrypt');
const salt=10;

const { ObjectId } = mongoose.Schema.Types

const Follow = require('./followModel')

const user = new mongoose.Schema({
    email:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    profile_img:{
        type: String,
        required: true
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    posts: [{
        type: ObjectId,
        ref: 'Article'
    }],
    followers: [{
        type: ObjectId,
        ref: 'User'
    }],
    following: [{
        type: ObjectId,
        ref: 'User'
    }],
    token:{
        type: String,
        default: ''
    }
})


user.statics.generateToken=function (id){
    var token =  jwt.sign({id},'123', {expiresIn: '1d'});
    return token
}

user.statics.findByToken=function(token){
    console.log('find by user', token)
    var isTokenVerified = false
    jwt.verify(token, '123',  (err, decoded) => {
         console.log('=dec ode=',decoded) // bar
        if(decoded){
            isTokenVerified = true
            return 
        }
    });
    return isTokenVerified

    // jwt.verify('123',function(err,decode){
    //     console.log('decode decode',decode)
    //     // users.findOne({"_id": decode, "token":token},function(err,user){
    //     //     if(err) return cb(err);
    //     //     cb(null,user);
    //     // })
    // })
};


module.exports = mongoose.model('User', user)