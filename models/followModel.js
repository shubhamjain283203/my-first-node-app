const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types
 
const followModel = new mongoose.Schema({
    followers: [{
        type: ObjectId,
        ref: 'User'
    }],
    following: [{
        type: ObjectId,
        ref: 'User'
    }],
    createdAt: {
        type: Date,
        default:Date.now
    },
})

module.exports = mongoose.model('Follow', followModel)