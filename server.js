const express = require('express')
const cors = require('cors');
const mongoose = require('mongoose')
const webpush = require('web-push')
const bodyParser = require('body-parser')
const path = require('path')
const jwt=require('jsonwebtoken');
// const metadata = require('gcp-metadata');
const {OAuth2Client} = require('google-auth-library');

const CONFIG = require("./config");

const User = require('./models/userModel')

// const oAuth2Client = new OAuth2Client();
// const cookieParser=require('cookie-parser');

var fs = require("fs");

const app = express()

const session = require('express-session');

const passport = require('passport');
var userProfile;

app.use(express.static(path.join(__dirname, "client")))
app.use(express.json())
app.use(cors())
app.use(passport.initialize());
app.use(passport.session());

// mongodb+srv://shubham:8349351712@cluster0.c57du.mongodb.net/blog?retryWrites=true&w=majority
// mongodb://localhost/blog

mongoose.connect('mongodb+srv://shubham:8349351712@cluster0.c57du.mongodb.net/blog?retryWrites=true&w=majority',{
    useNewUrlParser:true,
    useUnifiedTopology: true
},function(err, db) {
    console.log('connected')
})

// mongoose.connect('mongodb://localhost/blog',{
//     useNewUrlParser:true,
//     useUnifiedTopology: true
// },function(err, db) {
//     console.log('connected')
// })


const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const GOOGLE_CLIENT_ID = CONFIG.oauth2Credentials.client_id;
const GOOGLE_CLIENT_SECRET = CONFIG.oauth2Credentials.client_secret;

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    // callbackURL: "http://localhost:5000/oauth2callback",
    callbackURL: "https://my-first-blog-shubh.herokuapp.com/oauth2callback"
  },
  function(accessToken, refreshToken, profile, done) { 
      userProfile=profile;
      return done(null, userProfile);
  }
));

// mongoose.connect('mongodb://localhost/blog',{
//     useNewUrlParser:true,
//     useUnifiedTopology: true
// })

app.use(bodyParser.json())

app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'SECRET' 
}));

app.use(express.urlencoded({extended: true}))
app.use('/uploads',express.static('uploads'))
app.set('view engine', 'ejs')


const articleRouter = require('./routes/article')
const user = require('./routes/user')
const follow = require('./routes/follow')

const publicVapidKey = 'BCE_3AfqjEvFGx_4BpEEesLzkDHwxmHa00EzLAopqZre9_7gyfIxdu82EjoC-ZrQje_MAdiWvuCEvLeSPKtJzr8'
const privateVapidKey = 'EZb5Flv95EmZf47we9r7ZR8jwlRfAuotw0cPuW-xF4c'

webpush.setVapidDetails('mailto:test@test.com', publicVapidKey, privateVapidKey)

app.use('/user',user)
app.use('/article',articleRouter)
app.use('/follow', follow)

app.post('/subscribe', (req, res)=>{
        const subscription = req.body;
        console.log('subscription====',subscription)
        res.status(201).json({})
        const payload = JSON.stringify({'title': 'push test'})
        webpush.sendNotification(subscription, payload).catch(err=>{
            console.log('==',err)
    })
})

app.get('/',(req,res)=>{
    res.send({success: true, list: 'hi'})
})


app.get('/success', (req, res) => res.send(userProfile));

// const handleLogin = () => {
app.get('/createUser', async(req, res)=>{
    if(userProfile){
      console.log('=userProfile["_json"]=', userProfile["_json"]['email'])
      User.findOne({email: userProfile["_json"]['email']},  async (err, myUser) => {
          console.log('myUser',myUser); // That's very very weird, that's work but RARELY that's doesn't work
          if(!myUser){
              const user = new User({
                  email: userProfile["_json"]['email'],
                  name: userProfile["_json"]['name'],
                  isVerified: userProfile["_json"]['email_verified'],
                  profile_img: userProfile["_json"]['picture']
              })

              try{
                  await user.save((err, result)=>{
                          if(err){
                              res.send({success: false, msg: err})
                            } else{
                              res.send({success: true, list: result})
                            }
                      }
                  )
              }catch(err){
                  console.log(err)
              }
          }
        }
    )
    .exec((err,result)=>{
      console.log('result result',result)
        if(err) res.send({success: false, msg: err})
        if(result){
          res.send({success: true, list: result})
          // res.redirect('//onlinepadholikho.com/');
        }
    }) 
 
  }
  })



app.get('/error', (req, res) => res.send("error logging in"));

app.get('/auth/google', 
  passport.authenticate('google', { scope : ['profile', 'email'] }));
 
app.get('/oauth2callback', 
  passport.authenticate('google', { failureRedirect: '/error' }),
  (req, res) => {
    // Successful authentication, redirect success.
    // res.redirect('http://localhost:8080/#/loginsuccess');
    res.redirect('https://onlinepadholikho.com/#/loginsuccess');
 });

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});


app.listen( process.env.PORT || 5000)