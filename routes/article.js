const express = require('express')
const router = express.Router()
const multer = require('multer')

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/')
    },
    filename: function(req, file, cb){
        cb(null, file.originalname)
    }
})

const fileFilter = function(req, file, cb){
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true)
    }else{
        cb(null, false)
    }
}

const upload = multer({storage: storage,limits:{
    fileSize: 1024 * 1024 * 5
 },
 fileFilter
})


const Article = require('../models/articleModel')
const User = require('../models/userModel')

router.get('/',async (req,res)=>{
    const { page = 1, limit = 0 } = req.query;
    const count = await Article.countDocuments()
    if(req.query.value){
        Article.find({category: req.query.value})
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec((err,result)=>{
            if(err) res.send({success: false, msg: err})
            res.send({success: true, list: result,
                totalPages: Math.ceil(count / limit),
                currentPage: page
            })
        }) 
    }else{
        Article.find()
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec((err,result)=>{
            if(err) res.send({success: false, msg: err})
            res.send({success: true, list: result,
                totalPages: Math.ceil(count / limit),
                currentPage: page
            })
        }) 
    }
 })

router.post('/new', upload.single('articleImage') ,async(req, res)=>{
    const article = new Article({
        title: req.body.title,
        heading: req.body.heading,
        description: req.body.description,
        category: req.body.category,
        Author: req.body.Author,
        articleImage: req.file.path
    })

    try{
        await article.save((err, result)=>{
                if(err){
                    res.send({success: false, msg: err})
                } else{
                    User.findOneAndUpdate({ _id: req.body.Author}, { $push: { posts: result._id } }).then(data => {
                            res.send({success: true, result})
                    });
                }
            }
        )
    }catch(err){
        console.log(err)
    }
})

router.put('/comment',(req,res)=>{
    const comment = {
        text:req.body.text,
        postedBy:req.body.uid
    }
    Article.findByIdAndUpdate('5fd49922df60615ada79bc9f',{
        $push:{comments:comment}
    },{
        new:true
    })
    .populate("comments.postedBy","_id email")
    .populate("postedBy","_id email")
    .exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }else{
            res.json(result)
        }
    })
})

router.put('/like/:post_id',(req,res)=>{
    Article.findByIdAndUpdate(req.params.post_id,{
        $addToSet:{likes:[req.body.postedBy]}        
    },{
        new:true
    })
    .populate("likes","_id email")
    .exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }else{
            res.json(result)
        }
    })
})

router.get('/:post_id',(req,res)=>{
    Article.findById(req.params.post_id)
    .populate("Author","_id email name profile_img")
    .exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }else{
            res.json(result)
        }
    })
})





router.put('/unlike/:post_id',(req,res)=>{
    Article.findByIdAndUpdate(req.params.post_id,{
        $pull:{likes:req.body.postedBy}
    },{
        new:true
    })
    .populate("likes","_id email")
    .exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }else{
            res.json(result)
        }
    })
})

module.exports = router