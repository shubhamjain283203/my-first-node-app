const express = require('express')
const router = express.Router()
const User = require('../models/userModel')
const nodemailer = require('nodemailer'); 

async function mailSend(toSend, uid){
      const url = `http://localhost:5000/user/confirmation/${uid}`;

       var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'shubhamjain283203@gmail.com',
          pass: '8349351712fzd@'
        }
      });
      
      var mailOptions = {
        from: 'shubhamjain283203@gmail.com',
        to: toSend,
        subject: 'Sending Email using Node.js',
        html: `Please click this email to confirm your email: <a href="${url}">${url}</a>`,
    };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}

router.get('/confirmation/:uid',(req,res)=>{
    const {uid} = req.params
    console.log('sgjhkl',uid)
    User.findByIdAndUpdate(uid,{
        isVerified: true
    },{
        new:true
    })
    .exec((err,result)=>{
        if(err){
            return res.status(422).json({error:err})
        }else{
            res.json(result)
        }
    })
})


router.post('/login', async(req,res) => {
   if(req.headers.token === 'null'){ 
    let token =  await User.generateToken(req.headers.id);
    User.findByIdAndUpdate(req.headers.id,{
            token: token
        },{
            new:true
        })
        .exec((err,result)=>{
            if(err){
                return res.status(422).json({error:err})
            }else{
                res.json(result)
            }
        })
   }else{
    // let isTokenVerified = User.findByToken(req.headers.token)
    // console.log('isTokenVerifiedisTokenVerified',isTokenVerified)
    // if(!isTokenVerified){
    //     let token =  await User.generateToken(req.headers.id);
    //     console.log('==-=token==', token)
    //     User.findByIdAndUpdate(req.headers.id,{
    //             token: token
    //         },{
    //             new:true
    //         })
    //         .exec((err,result)=>{
    //             if(err){
    //                 return res.status(422).json({error:err})
    //             }else{
    //                 res.json(result)
    //             }
    //     })
    //     return
    // }
     res.status(400).json({error: false, msg: 'Already have token'})
   }
});

router.get('/logout', function(req,res){
    User.findByIdAndUpdate(req.headers.id,{
            token: null
        },{
            new:true
        })
        .exec((err,result)=>{
            if(err){
                return res.status(422).json({error:err})
            }else{
                res.json(result)
            }
        })
}); 

// router.get('/',(req,res)=>{
//     // .populate("followers","_id email")
//     // .populate("following","_id email")
//     User.find((err, result)=>{ 
//         if(err) res.send({success: false, msg: err})
//         res.send({success: true, list: result})
//     })
// })

router.get('/',(req,res)=>{
    User.find()
    .populate("posts", "_id" )
    .populate("followers","_id email")
    .populate("following","_id email")
    .exec((err,result)=>{
        if(err) res.send({success: false, msg: err})
        res.send({success: true, list: result})
    }) 
 }) 


router.post('/',async(req, res)=>{
    console.log('req',req.body)

    const user = new User({
        email: req.body.email,
        password: req.body.password
    })

    try{
        await user.save((err, result)=>{
                if(err) res.send({success: false, msg: err})
                res.send({success: true, result: result})
                mailSend(req.body.email, result._id)
            }
        )
    }catch(err){
        console.log(err)
    }
})

router.put('/follow/:id',async (req,res)=>{
    if(req.body.action === 'follow'){
        let follower, following;
        try {
            follower =  await User.findByIdAndUpdate(req.params.id,{
                     $addToSet:{followers:[req.body.followedBy]}        
                 },{
                     new:true
                 }
             )
             .populate("followers","_id email")
            
              following =  await  User.findByIdAndUpdate(req.body.followedBy,{
                     $addToSet:{following:[req.params.id]}
                 },{
                     new:true
                 }
             )
             .populate("following","_id email")
            
        }catch(error){
            console.log('=shjkl')
            return next(error);
        }

        return res.send({follower, following})

    }else{
        let follower, following;
        try {
            follower =  await User.findByIdAndUpdate(req.params.id,{
                 $pull:{
                     followers:req.body.followedBy,
                     },
                 },{
                     new:true
                 }
             )
             .populate("followers","_id email")
            
              following =  await  User.findByIdAndUpdate(req.body.followedBy,{
                 $pull:{
                     following:req.params.id,
                     },
                 },{
                     new:true
                 }
             )
             .populate("following","_id email")
            
        }catch(error){
            console.log('=shjkl')
            return next(error);
        }

        return res.send({follower, following})

    }

})

module.exports = router