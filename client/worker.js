self.addEventListener('push', function(event) {
    console.log('Received a push message', event);
    const data = event.data.json();
    console.log('=push Received==', data)
    var title = 'Yay a message.';
    var body = 'We have received a push message.';
    var icon = './pwa.png';
    var tag = 'simple-push-demo-notification-tag';
  
    event.waitUntil(
      self.registration.showNotification(title, {
        body: body,
        icon: icon,
        tag: tag
      })
    );
  });